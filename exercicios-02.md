## 2 - Busca e substituição em um arquivo HTML

Eu tenho o seguinte script:

```
#!/bin/bash

url='https://www.gnu.org/software/bash/manual/bash.html'
src='bash-index.txt'

declare -A toc

while read line; do
    toc[${line% *}]="${line##* }"
done < $src

sel=$(fzf <<< $(printf '%s\n' "${!toc[@]}"))

w3m $url${toc[$sel]}
```


Mas, para que ele funcione, eu preciso obter uma lista de tópicos e suas respectivas âncoras do manual oficial do Bash e salvá-la no arquivo ~bash-index.txt~.

Complete a linha do ~sed~, abaixo, com uma expressão regular que encontre os títulos dos tópicos e suas respectivas âncoras:

```
sed -En 's|REGEX|\2 \1|p'
```

### Resposta para um site diferente que eu fiz para extrair apenas as vídeo-aulas

```
wget https://kretcheu.com.br/redes-t2/ -qO - |sed -En 's|.*<td>.*href="([^"]+mp4).*|\1|p' > kretcheu-videoaulas.txt
```