#!/usr/bin/env bash


# Variável contendo todas as palavras que serão testadas---
palavras=(cana banana ponte 'Belo Horizonte' bandana entulho bagulho bacana contente patente colante savana)

# Habilitar extglog por estar no modo não interativo
 shopt -s extglob

# Principal -----------------------------------------------
echo "A seguite lista de palavras: ${palavras[@]}, será dividido de acordo com grupo de padrões:"
echo
for word in "${palavras[@]}"; do
    case $word in
        b*) echo "- $word casa com o padrão: 'b*'";;
        *lho) echo "- $word casa com o padrão: '*lho'";;
        @(c)*!(te)) echo "- $word casa com o padrão: '@(c)*!(te)'";;
        @(c)*@(te)) echo "- $word casa com o padrão: '@(c)*@(te)'";;
        +(p|s|B)*?(H)*+(nte|ana)) echo "- $word casa com o padrão: +(p|s|b)*?(H)*+(nte|ana)"
    esac
done
echo