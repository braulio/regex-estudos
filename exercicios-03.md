## 3 - Resolução dos monitores conectados

Observe, abaixo, como eu estou usando o ~xrandr~:

```
xrandr --nograb --current
```

Ele me dá a seguinte saída:

```
Screen 0: minimum 320 x 200, current 1920 x 1080, maximum 16384 x 16384
VGA-1 disconnected (normal left inverted right x axis y axis)
HDMI-1 connected primary 1920x1080+0+0 (normal left inverted right x axis y axis) 160mm x 90mm
   1920x1080     60.00*+  50.00    59.94    30.00    25.00    24.00    29.97    23.98  
   1920x1080i    60.00    50.00    59.94  
   1280x1024     60.02  
   1360x768      60.02  
   1280x720      60.00    50.00    59.94  
   1024x768      60.00  
   800x600       60.32  
   720x576       50.00  
   720x576i      50.00  
   720x480       60.00    59.94  
   640x480       60.00    59.94  
   720x400       70.08  
DP-1 disconnected (normal left inverted right x axis y axis)
```

Repare que, ao lado de cada dispositivo de vídeo, nós temos as palavras ~connected~ e ~disconnected~. Com base nisso, complete a /pipeline/ abaixo para que eu tenha na saída o nome de cada dispositivo conectado seguido de sua resolução, por exemplo:

```
:~$ echo $my_res
VGA-1:1920x1080 HDMI-1:1920x1080
```

*A pipeline:*

```
my_res=$(echo $(xrandr --nograb --current | sed -nE 's/(.*) connected.* ([0-9]x[0-9]+).*/\1: \2/p'))
```

## 4 - Tema GTK e de ícones

Considerando as minhas configurações no arquivo abaixo:

```
:~$ cat ~/.config/gtk-3.0/settings.ini 
[Settings]
gtk-theme-name=Arc-Dark
gtk-icon-theme-name=debxp-paper-folders
gtk-font-name=Open Sans 10
gtk-cursor-theme-name=breeze_cursors
gtk-cursor-theme-size=0
gtk-toolbar-style=GTK_TOOLBAR_BOTH
gtk-toolbar-icon-size=GTK_ICON_SIZE_LARGE_TOOLBAR
gtk-button-images=1
gtk-menu-images=1
gtk-enable-event-sounds=1
gtk-enable-input-feedback-sounds=1
gtk-xft-antialias=1
gtk-xft-hinting=1
gtk-xft-hintstyle=hintfull
gtk-xft-rgba=rgb
gtk-decoration-layout=appmenu
```

Complete as expressões regulares abaixo para obter o tema GTK e o tema de ícones nas variáveis abaixo:

```
# Meu arquivo de configurações...
gtk_settings=$HOME/.config/gtk-3.0/settings.ini

# As variáveis...
my_theme=$(grep -Po 'gtk-theme-name=\k.*' $gtk_settings)
my_icons=$(grep -Po 'gtk-icon-theme-name=\k.*' $gtk_settings)
```

Resultado esperado:

```
:~$ echo $my_theme
Arc-Dark
:~$ echo $my_icons
debxp-paper-folders
```

## 5 - Nome da distribuição

Considere a saída do comando abaixo:

```
:~$ grep PRETTY /etc/os-release
# PRETTY_NAME="Debian GNU/Linux bookworm/sid"
PRETTY_NAME="Debian GNU/Linux-libre (sid)"
```

Complete a REGEX abaixo de modo a obter a seguinte saída:

```
:~$ sed -nE 's/^P.*="(.*)\s\((.*)\).*/\1\/\2/p' /etc/os-release
Debian GNU/Linux-libre/sid
```
