#!/usr/bin/env bash


# Variável contendo todas as palavras que serão testadas---
palavras=(cana banana ponte 'Belo Horizonte' bandana entulho bagulho bacana contente patente colante savana)

# Principal -----------------------------------------------
for word in "${palavras[@]}"; do
    [[ $word == *+(onte|tulho) ]] && echo "+ ${word}: casa com as palavras ponte 'Belo Horizonte' e entulho" || echo "- ${word}: não casa com as palavras ponte 'Belo Horizonte' e entulho"
    done